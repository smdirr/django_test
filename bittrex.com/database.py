import sqlite3
import os
import logging


def get_conn():
    dbname = os.path.join(os.getcwd(), 'api', 'db.sqlite3')
    logging.info(f"Connecting ... {dbname}")
    return sqlite3.connect(dbname)

def create_table_prices(con):
    cursorObj = con.cursor()
    cursorObj.execute("CREATE TABLE if not exists prices_price(symbol text, high real, low real, volume real, quoteVolume real, percentChange real, updatedAt text)")
    con.commit()

def insert_table(con, sql_insert):
    cursorObj = con.cursor()
    cursorObj.execute(sql_insert)
    con.commit()