import os
import sys
import logging
import json
import requests
import time
from database import get_conn, create_table_prices, insert_table


def get_config(cfg_file):   
    with open(cfg_file, 'r') as file:
        config = json.load(file)
    return config


def data_to_insert(json_data):
    sql = "INSERT INTO prices_price (symbol, high, low, volume, quoteVolume, percentChange, updatedAt)" \
        " VALUES('{}',{},{},{},{},{},'{}')".format(json_data['symbol'], json_data['high'], json_data['low'], 
        json_data['volume'], json_data['quoteVolume'], json_data['percentChange'], json_data['updatedAt'])
    return sql


def main(config_file):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(levelname)s-%(message)s')
    logging.info("Init extract process")
    
    con = get_conn()

    config = get_config(config_file)

    r = requests.get(config['URL_API'])
    
    if r.ok:
        logging.info(f"Results {r.json()}")
        sql_results = data_to_insert(r.json())
        insert_table(con, sql_results)
        logging.info("Data saved at database")
    else:
        logging.error(f"Error {r.status_code} {r.text}")


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Falta parámetro fichero de configuración")
        sys.exit(1)

    config_file = sys.argv[1]
   
    try:
        while True:
            main(config_file)
            time.sleep(60)
    except KeyboardInterrupt:
        pass
