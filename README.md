# Ejercicio #

## Instalación ##

* `python -m venv venv`
* Activar entorno `venv\Scripts\activate` en windows o `. venv/bin/activate` en linux
* `pip install -r requirements`

## API ##

python .\manage.py migrate
python .\manage.py runserver

comprobar funcionamiento http://127.0.0.1:8000/price/

## Script ##
El script `bittrex.com/extract.py` cada un minuto extrae datos de una URL y los guarda en una DB.

## Ejecución script ##
Recibe como parámetro el fichero de configuración `.\bittrex.com\settings.json`

* activar venv
* `python .\bittrex.com\extract.py .\bittrex.com\settings.json`
* Detener con ctrl + c