from django.db import models


class Price(models.Model):
    symbol = models.CharField(max_length=20, blank=False, default='')
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    quoteVolume = models.FloatField()
    percentChange = models.FloatField()
    updatedAt = models.CharField(max_length=30, blank=False, default='')

    class Meta:
        ordering = ['-updatedAt']
