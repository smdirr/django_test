from .models import Price
from rest_framework import serializers


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ['id', 'symbol', 'high', 'low', 'volume', 'quoteVolume', 'percentChange', 'updatedAt']
